﻿using System;
namespace asciitest
{
	/**
	 * Creates a 'bound' place which the player cannot enter. 
	 */
    public class Bound
    {
		public char BoundChar { get; set; } = ' ';
		public int StartX { get; set; } = 0;
		public int EndX { get; set; } = 0;
		public int StartY { get; set; } = 0;
		public int EndY { get; set; } = 0;
		public ConsoleColor BackgroundColour { get; set; } = ConsoleColor.Black;
		public ConsoleColor ForegroundColour { get; set; } = ConsoleColor.Gray;

		public Bound(int startX, int endX, int startY, int endY, char icon,
		             ConsoleColor forecolour, ConsoleColor backcolour)
		{
			this.StartX = startX;
			this.EndX = endX;
			this.StartY = startY;
			this.EndY = endY;
			this.BoundChar = icon;
			this.BackgroundColour = backcolour;
			this.ForegroundColour = forecolour;
		}
    }
}
