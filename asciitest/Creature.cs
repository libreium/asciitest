﻿using System;
namespace asciitest
{
    public class Creature
    {
		public enum FriendType{ Neutral, Friend, Enemy };

		public char DisplayChar { get; set; } = '.';
        public int X { get; set; } = 0;
        public int Y { get; set; } = 0;
        public bool isPassible { get; set; } = true;
        public ConsoleColor DisplayForeColour { get; set; } = ConsoleColor.Gray;
        public ConsoleColor DisplayBackColour { get; set; } = ConsoleColor.Black;
		public bool CanIde { get; set; } = true;
		public bool IsHostile { get; set; } = false;
		public FriendType FriendState { get; set; } = FriendType.Neutral;

		public int Health { get; set; } = 100;

        public Creature()
        {
        }

        public Creature(int x, int y, char c, ConsoleColor foreColour,
                        ConsoleColor backColour)

        {
            this.DisplayChar = c;
            this.X = x;
            this.Y = y;
            this.DisplayForeColour = foreColour;
            this.DisplayBackColour = backColour;
        }
    }
}
