﻿using System;
namespace asciitest
{
	public class Item
	{
		public int X { get; set; } = 0;
		public int Y { get; set; } = 0;
		public int Value { get; set; } = 0;
		public int Weight { get; set; } = 0;
		public char ItemChar { get; set; } = ' ';
		public string Description { get; set; } = "Item";
		public int HealthMod { get; set; } = 0;
		public int EitherMod { get; set; } = 0;
		public int StrengthMod { get; set; } = 0;
		public int NumberOfItemsOwned { get; set; } = 1;
		public int DamageMod { get; set; } = 0;
		public int ArmourMod { get; set; } = 0;
		public ConsoleColor BackgroundColour { get; set; } = ConsoleColor.Black;
		public ConsoleColor ForegroundColour { get; set; } = ConsoleColor.Gray;

		public Item()
		{
		}

		public Item(int x, int y, char itemChar, string description)
		{
			this.X = x;
			this.Y = y;
			this.ItemChar = itemChar;
			this.Description = description;
		}

		public void AddItem()
		{
			NumberOfItemsOwned++;
		}

		public void RemoveItem()
		{
			if (NumberOfItemsOwned == 1)
			{
				
			}
			else
			{
				NumberOfItemsOwned--;
			}
		}
	}
}
