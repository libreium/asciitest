﻿using System;
using System.Text;
using System.Collections;
using System.Threading;

namespace asciitest
{
    class MainClass
    {
		static ArrayList objects = new ArrayList();
		static ArrayList Items = new ArrayList();
		static ArrayList creatures = new ArrayList();
		static int displayPortHeight = Console.WindowHeight - 3;
		static int displayPortWidth = Console.WindowWidth - 50;
		static Creature player = new Creature(1, 1, '@', ConsoleColor.White,
											  ConsoleColor.Black);

		static Creature testCreature = new Creature(3, 9, 'S'
                                    ,ConsoleColor.DarkRed, ConsoleColor.Black);
		static CharObject testObject = new CharObject(3, 10, '+',false, 
		                             ConsoleColor.Magenta, ConsoleColor.Black);

		private static void Test()
		{
			Console.SetCursorPosition(1, Console.WindowHeight - 1);
			for (int i = 0; i <= 5; i++)
			{
				Console.BackgroundColor = ConsoleColor.Blue;
				Console.Write(' ');
			}
			Console.BackgroundColor = ConsoleColor.Black;
		}

		static void GenObjects()
		{
			Random random = new Random();
			for (int i = 0; i <= 100; i++)
			{
				int ry = (int)random.Next(displayPortHeight);
				CharObject charObject = new CharObject(
					random.Next(displayPortWidth),ry,'T', false, 
					ConsoleColor.Green, ConsoleColor.Black);

				objects.Add(charObject);
			}
		}

        static void AddBound(Bound bound)
		{
			
		}

		private static void PrintOjects()
		{
			foreach (CharObject c in objects)
			{
				Console.Title = (c.X + " " + c.Y);
				if (c.X >= 0 && c.X <= displayPortWidth && c.Y >= 0 && c.Y
					<= displayPortHeight)
				{
					Console.SetCursorPosition(c.X, c.Y);
					Console.ForegroundColor = c.DisplayForeColour;
					Console.BackgroundColor = c.DisplayBackColour;
					Console.Write(c.DisplayChar);
					Console.BackgroundColor = ConsoleColor.Black;
					Console.ForegroundColor = ConsoleColor.Gray;
				}
			}
		}

		static void PrintItems()
		{
			foreach(Item item in Items)
			{
				if (item.X >= 0 && item.X <= displayPortWidth && item.Y > 0 
				    && item.Y <= displayPortHeight)
				{
					Console.SetCursorPosition(item.X, item.Y);
					Console.ForegroundColor = item.ForegroundColour;
					Console.BackgroundColor = item.BackgroundColour;
					Console.Write(item.ItemChar);
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.Gray;
				}
			}
		}

		public static void PrintCreatures()
		{
			foreach(Creature creature in creatures)
			{
				if (creature.X >= 0 && creature.X <= displayPortWidth && 
				    creature.Y > 0 && creature.Y <= displayPortHeight)
				{
					Console.SetCursorPosition(creature.X, creature.Y);
					Console.ForegroundColor = creature.DisplayForeColour;
					Console.BackgroundColor = creature.DisplayBackColour;
					Console.Write(creature.DisplayChar);
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.Gray;
				}
			}
		}

		public static void ClearScreen()
		{
			Console.Clear();
			Console.BackgroundColor = ConsoleColor.Black;

			for (int i = 0; i <= 1000; i++)
			{
				Console.Write(' ');
			}
			Console.Clear();
		}

		public static ArrayList GetItemUnderPlayer()
		{
			ArrayList listOut = new ArrayList();
			foreach(Item item in Items)
			{
				if (item.X == player.X && item.Y == player.Y)
				{
					listOut.Add(item);
				}
			}
			return listOut;
		}

        public static void Main(string[] args)
        {
			CharObject testObj1 = new CharObject(13,13,'1',false,
			                                     ConsoleColor.Cyan,
			                                     ConsoleColor.Black);
			CharObject testObj2 = new CharObject(20, 20, '2', false,
                                     ConsoleColor.Cyan,
                                     ConsoleColor.Black);

			objects.Add(testObj1);
			objects.Add(testObj2);
			objects.Add(testObject);

			Item testItem1 = new Item(32, 16, '#', "item 1");
			Item testItem2 = new Item(32, 15, '$', "item 12");
			Items.Add(testItem1);
			Items.Add(testItem2);
            
			testCreature.FriendState = Creature.FriendType.Friend;
			Thread thread = new Thread(ThreadTest);
			thread.Start();

			creatures.Add(testCreature);

			//printChars();
			GenObjects();
			ClearScreen();

			int ex = 5;
			int ey = 5;
			int em = 0;

			if (player.X == ex && player.Y == ey ){
				Environment.Exit(0);
			}

            Console.WriteLine("Hello World!");
			Console.Clear();
			Console.CursorVisible = false;
			while (true)
			{
				//Console.Clear();
				//Console.Write("\r" + new string(' ', Console.WindowWidth) + "\r");
				PrintOjects();
				PrintItems();
				PrintCreatures();

				ArrayList itemsUnderPlayer = GetItemUnderPlayer();
				foreach(Item item in itemsUnderPlayer)
				{
					Console.Write(" " + item.Description + " ");
				}

				Test();

				Console.SetCursorPosition(ex, ey);
				Console.ForegroundColor = ConsoleColor.Red;
				Console.Write((char)em);
				Console.ForegroundColor = ConsoleColor.Gray;
				em++;

				Console.SetCursorPosition(player.X, player.Y);
				Console.BackgroundColor = player.DisplayBackColour;
				Console.ForegroundColor = player.DisplayForeColour;
				Console.Write(player.DisplayChar);

				ConsoleKeyInfo cki = Console.ReadKey(true);
				char c = cki.KeyChar;

				if (c == '0')
				{
					Environment.Exit(0);
				}

				if (c == 'd'){
					if (CanMoveEast())
					{
						Console.SetCursorPosition(player.X, player.Y);
                        Console.Write(' ');
						player.X++;
						Console.SetCursorPosition(player.X, player.Y);
						Console.BackgroundColor = player.DisplayBackColour;
						Console.ForegroundColor = player.DisplayForeColour;
                        Console.Write(player.DisplayChar);
					}
				}
                
				if (c == 's')
                {
					if (CanMoveSouth())
					{
						Console.SetCursorPosition(player.X, player.Y);
                        Console.Write(' ');
						player.Y++;
						Console.SetCursorPosition(player.X, player.Y);
						Console.BackgroundColor = player.DisplayBackColour;
                        Console.ForegroundColor = player.DisplayForeColour;
                        Console.Write(player.DisplayChar);
					}
                }

				if (c == 'a')
                {
					if (CanMoveWest())
					{
						Console.SetCursorPosition(player.X, player.Y);
                        Console.Write(' ');
						player.X--;
						Console.SetCursorPosition(player.X, player.Y);
						Console.BackgroundColor = player.DisplayBackColour;
                        Console.ForegroundColor = player.DisplayForeColour;
                        Console.Write(player.DisplayChar);
					}
                }

				if (c == 'w')
                {
					if (CanMoveNorth())
					{
						Console.SetCursorPosition(player.X, player.Y);
                        Console.Write(' ');
						player.Y--;
						Console.SetCursorPosition(player.X, player.Y);
						Console.BackgroundColor = player.DisplayBackColour;
                        Console.ForegroundColor = player.DisplayForeColour;
						Console.Write(player.DisplayChar);
					}
                }
			}
        }

		public static bool CanMoveEast()
		{
			if (player.X == displayPortWidth - 1)
			{
				return false;
			}

			foreach(CharObject c in objects)
			{
				if (c.X == player.X + 1 && c.Y == player.Y 
				    && c.IsPassible == false)
				{
					return false;
				}
			}
			return true;
		}

		public static bool CanMoveWest()
		{
			if (player.X == 0)
			{
				return false;	
			}

			foreach (CharObject c in objects)
            {
                if (c.X == player.X - 1 && c.Y == player.Y
                    && c.IsPassible == false)
                {
                    return false;
                }
            }
			return true;
		}

		public static bool CanMoveNorth()
		{
            if (player.Y == 0)
			{
				return false;
			}

			foreach (CharObject c in objects)
            {
                if (c.Y == player.Y - 1 && c.X == player.X
                    && c.IsPassible == false)
                {
                    return false;
                }
            }
          
			return true;
		}

		public static bool CanMoveSouth()
        {
			if (player.Y == displayPortHeight)
            {
                return false;
            }

            foreach (CharObject c in objects)
            {
                if (c.Y == player.Y + 1 && c.X == player.X
                    && c.IsPassible == false)
                {
                    return false;
                }
            }

            return true;
        }

		public static void ThreadTest()
		{
			bool isMovingUp = true;
			while(true)
			{
				Console.SetCursorPosition(testObject.X, testObject.Y);
                Console.Write(' ');

                if (isMovingUp)
                {
					testObject.X++;
                }
                else
                {
					testObject.X--;
                }

				if (testObject.X == 8)
				{
					isMovingUp = false;
				}
				else if (testObject.X == 3)
				{
					isMovingUp = true;
				}
                
                PrintCreatures();
				//PrintOjects();
				Thread.Sleep(300);
			}
        }   
    } 
}

/*
 *Console.WriteLine("Press ESC to stop");
do {
    while (! Console.KeyAvailable) {
        // Do something
   }       
} while (Console.ReadKey(true).Key != ConsoleKey.Escape
*/
