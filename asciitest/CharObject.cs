﻿using System;
namespace asciitest
{
    public class CharObject
    {
		public char DisplayChar { get; set; } = '.';
		public int X { get; set; } = 0;
		public int Y { get; set; } = 0;
		public bool IsPassible { get; set; } = true;
		public ConsoleColor DisplayForeColour { get; set; } = ConsoleColor.Gray;
		public ConsoleColor DisplayBackColour { get; set; } = ConsoleColor.Black;

        public CharObject()
        {
        }

		public CharObject(int x, int y, char c, bool isPassible, 
		                  ConsoleColor foreColour, ConsoleColor backColour)
		{
			this.DisplayChar = c;
			this.X = x;
			this.Y = y;
			this.IsPassible = isPassible;
			this.DisplayForeColour = foreColour;
			this.DisplayBackColour = backColour;
		}
    }
}
